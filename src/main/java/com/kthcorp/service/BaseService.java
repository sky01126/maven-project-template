/*
 * Copyright ⓒ [2017] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */
package com.kthcorp.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import com.kthcorp.conf.Version;
import com.kthcorp.util.FileUtils;
import com.kthcorp.util.IOUtils;

/**
 * BaseService.java
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @see
 * @since 7.0
 */
public class BaseService {

	protected static final String BASE_PATH = "/META-INF/template";

	protected static final String EXTENSION = ".txt";

	protected String path;

	protected String groupId;

	protected String artifactId;

	protected String name;

	protected String description;

	protected File baseDir;

	protected File parentDir;

	protected File wasDir;

	protected File wasMainDir;

	protected File wasMainResourcesDir;

	protected File wasTestDir;

	protected File wasTestResourcesDir;

	protected void mkdirs(String path, String artifactId) {
		this.baseDir = FileUtils.getFile(path, artifactId);
		FileUtils.mkdirs(baseDir);

		// parent 디렉토리 생성
		parentDir = FileUtils.getFile(baseDir, artifactId + "-parent");
		FileUtils.mkdirs(parentDir);

		// parent 디렉토리 생성
		this.wasDir = FileUtils.getFile(baseDir, artifactId + "-was-default");
		FileUtils.mkdirs(wasDir);

		this.wasMainDir = FileUtils.getFile(wasDir, "src/main/java");
		FileUtils.mkdirs(wasMainDir);

		this.wasMainResourcesDir = FileUtils.getFile(wasDir, "src/main/resources");
		FileUtils.mkdirs(wasMainResourcesDir);

		FileUtils.mkdirs(FileUtils.getFile(wasDir, "src/main/webapp/resources"));
		FileUtils.mkdirs(FileUtils.getFile(wasDir, "src/main/webapp/WEB-INF/views"));

		this.wasTestDir = FileUtils.getFile(wasDir, "src/test/java");
		FileUtils.mkdirs(wasTestDir);

		this.wasTestResourcesDir = FileUtils.getFile(wasDir, "src/test/resources");
		FileUtils.mkdirs(wasTestResourcesDir);
	}

	protected String replaceAll(InputStream in) throws IOException {
		String content = IOUtils.toString(in);
		content = content.replaceAll("\\{GROUP_ID\\}", groupId);
		content = content.replaceAll("\\{GROUP_ID_PATH\\}", groupId.replaceAll("\\.", "/"));
		content = content.replaceAll("\\{ARTIFACT_ID\\}", artifactId);
		content = content.replaceAll("\\{VERSION\\}", Version.PROJECT);
		content = content.replaceAll("\\{NAME\\}", name);
		content = content.replaceAll("\\{DESCRIPTION\\}", description);
		content = content.replaceAll("\\{SPRING_BOOT_VERSION\\}", Version.SPRING_BOOT);
		content = content.replaceAll("\\{JAVA_VERSION\\}", Version.JAVA);
		content = content.replaceAll("\\{CGLIB_VERSION\\}", Version.CGLIB);
		content = content.replaceAll("\\{COMMONS_LANG3_VERSION\\}", Version.COMMONS_LANG3);
		content = content.replaceAll("\\{EHCACHE_SPRING_ANNOTATIONS_VERSION\\}", Version.EHCACHE_SPRING);
		content = content.replaceAll("\\{GUAVA_VERSION\\}", Version.GUAVA);
		content = content.replaceAll("\\{HIKARI_CP_VERSION\\}", Version.HIKARI_CP);
		content = content.replaceAll("\\{JAKARTA_REGEXP_VERSION\\}", Version.JAKARTA_REGEXP);
		content = content.replaceAll("\\{JODA_TIME_VERSION\\}", Version.JODA_TIME);
		content = content.replaceAll("\\{LOG4JDBC_LOG4J2_JDBC4_VERSION\\}", Version.LOG4JDBC_LOG4J2_JDBC4);
		content = content.replaceAll("\\{TOMCAT_EMBED_VERSION\\}", Version.TOMCAT_EMBED);
		content = content.replaceAll("\\{MYBATIS_VERSION\\}", Version.MYBATIS);
		content = content.replaceAll("\\{MYBATIS_SPRING_VERSION\\}", Version.MYBATIS_SPRING);
		content = content.replaceAll("\\{SQLJDBC4_VERSION\\}", Version.SQLJDBC4);
		content = content.replaceAll("\\{MYSQL_VERSION\\}", Version.MYSQL);
		content = content.replaceAll("\\{POSTGRESQL_VERSION\\}", Version.POSTGRESQL);
		content = content.replaceAll("\\{LOMBOK_VERSION\\}", Version.LOMBOK);
		return content;
	}

}
