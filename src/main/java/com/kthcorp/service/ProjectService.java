/*
 * Copyright ⓒ [2017] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */
package com.kthcorp.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import com.kthcorp.util.CompressUtils;
import com.kthcorp.util.FileUtils;
import com.kthcorp.util.IOUtils;

/**
 * ProjectService
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @date 2017. 2. 2.
 * @see
 * @since 7.0
 */
public class ProjectService extends BaseService {

	public ProjectService(String path, String groupId, String artifactId, String name, String description)
			throws IOException {
		this.path = path;
		this.groupId = groupId;
		this.artifactId = artifactId;
		this.name = name;
		this.description = description;

		// 기본 디렉토리 생성.
		FileUtils.delete(FileUtils.getFile(path, artifactId + ".tar.gz"));
		FileUtils.deleteAll(FileUtils.getFile(path, artifactId));
		mkdirs(path, artifactId);
	}

	public void createBase() throws IOException {
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/pom.xml" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(baseDir, "pom.xml"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
		in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/gitignore" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(baseDir, ".gitignore"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}

		// Parent POM
		in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/parent/pom.xml" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(parentDir, "pom.xml"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}

		// WAS POM
		in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/was/pom.xml" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(wasDir, "pom.xml"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
		in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/was/deploy.xml" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(wasDir, "deploy.xml"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	public void createSource() throws IOException {
		// Java File 생성.
		application(FileUtils.getFile(wasMainDir, groupId.replaceAll("\\.", "/")));

		// Property 생성
		property(wasMainResourcesDir);

		// JSP File 생성.
		views(FileUtils.getFile(wasDir, "/src/main/webapp/WEB-INF/views"));
	}

	/**
	 * Create Java File.
	 *
	 * @param saveDir 저장경로.
	 * @throws IOException
	 */
	private void application(File saveDir) throws IOException {
		// Main
		FileUtils.mkdirs(saveDir);
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/was/main/java/Application.java" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "Application.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}

		// Config
		FileUtils.mkdirs(FileUtils.getFile(saveDir, "/config"));
		in = null;
		try {
			in = this.getClass()
					.getResourceAsStream(BASE_PATH + "/was/main/java/config/LifecycleConfiguration.java" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/config/LifecycleConfiguration.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
		in = null;
		try {
			in = this.getClass()
					.getResourceAsStream(BASE_PATH + "/was/main/java/config/WebMvcConfiguration.java" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/config/WebMvcConfiguration.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}

		// WEB Main Persistence DAO
		FileUtils.mkdirs(FileUtils.getFile(saveDir, "/persistence/dao"));
		in = null;
		try {
			in = this.getClass()
					.getResourceAsStream(BASE_PATH + "/was/main/java/persistence/dao/BaseDao.java" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/persistence/dao/BaseDao.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
		in = null;
		try {
			in = this.getClass()
					.getResourceAsStream(BASE_PATH + "/was/main/java/persistence/dao/TestDao.java" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/persistence/dao/TestDao.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}

		// Property
		FileUtils.mkdirs(FileUtils.getFile(saveDir, "/property"));
		in = null;
		try {
			in = this.getClass()
					.getResourceAsStream(BASE_PATH + "/was/main/java/property/BuildInfoProperty.java" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/property/BuildInfoProperty.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}

		// Service
		FileUtils.mkdirs(FileUtils.getFile(saveDir, "/service"));
		in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/was/main/java/service/TestService.java" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/service/TestService.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}

		// Controller
		FileUtils.mkdirs(FileUtils.getFile(saveDir, "/web/controller"));
		in = null;
		try {
			in = this.getClass()
					.getResourceAsStream(BASE_PATH + "/was/main/java/web/controller/TestController.java" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/web/controller/TestController.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}

		// WEB Main Filter
		FileUtils.mkdirs(FileUtils.getFile(saveDir, "/web/filter"));
		in = null;
		try {
			in = this.getClass()
					.getResourceAsStream(BASE_PATH + "/was/main/java/web/filter/AllowFilter.java" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/web/filter/AllowFilter.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}

		// WEB Main Handler
		FileUtils.mkdirs(FileUtils.getFile(saveDir, "/web/handler"));
		in = null;
		try {
			in = this.getClass().getResourceAsStream(
					BASE_PATH + "/was/main/java/web/handler/BaseExceptionHandler.java" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/web/handler/BaseExceptionHandler.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
		in = null;
		try {
			in = this.getClass().getResourceAsStream(
					BASE_PATH + "/was/main/java/web/handler/GlobalControllerExceptionHandler.java" + EXTENSION);
			String content = replaceAll(in);

			FileUtils.write(FileUtils.getFile(saveDir, "/web/handler/GlobalControllerExceptionHandler.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}

		FileUtils.mkdirs(FileUtils.getFile(saveDir, "/web/util"));
		in = null;
		try {
			in = this.getClass()
					.getResourceAsStream(BASE_PATH + "/was/main/java/web/util/RequestUtils.java" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/web/util/RequestUtils.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
		in = null;
		try {
			in = this.getClass()
					.getResourceAsStream(BASE_PATH + "/was/main/java/web/util/ResponseUtils.java" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/web/util/ResponseUtils.java"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	/**
	 * Create Spring Boot Application YAML File.
	 *
	 * @param saveDir 저장경로.
	 * @throws IOException
	 */
	private void property(File saveDir) throws IOException {
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/was/main/resources/application.yml");
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "application.yml"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
		// InputStream in = null;
		// try {
		// in = this.getClass().getResourceAsStream(BASE_PATH + "/was/main/resources/application.properties");
		// String content = replaceAll(in);
		// FileUtils.write(FileUtils.getFile(saveDir, "application.properties"), content);
		// } finally {
		// IOUtils.close(in);
		// }
		in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/was/main/resources/logback.xml" + EXTENSION);
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "logback.xml"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	/**
	 * Create Error Jsp Page File.
	 *
	 * @param saveDir 저장경로.
	 * @throws IOException
	 */
	private void views(File saveDir) throws IOException {
		FileUtils.mkdirs(FileUtils.getFile(saveDir, "/error"));
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/was/main/webapp/WEB-INF/views/main.jsp");
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/main.jsp"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
		in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/was/main/webapp/WEB-INF/views/error/400.jsp");
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/error/400.jsp"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
		in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/was/main/webapp/WEB-INF/views/error/404.jsp");
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/error/404.jsp"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
		in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/was/main/webapp/WEB-INF/views/error/405.jsp");
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/error/405.jsp"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
		in = null;
		try {
			in = this.getClass().getResourceAsStream(BASE_PATH + "/was/main/webapp/WEB-INF/views/error/error.jsp");
			String content = replaceAll(in);
			FileUtils.write(FileUtils.getFile(saveDir, "/error/error.jsp"), content);
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	/**
	 * 프로젝트를 tar.gz으로 압축한다.
	 *
	 * @throws IOException
	 */
	public void compress() throws IOException {
		CompressUtils.createTarGZ(baseDir, FileUtils.getFile(path, artifactId + ".tar.gz"));

		// 원본 프로젝트 삭제.
		FileUtils.deleteAll(FileUtils.getFile(path, artifactId));
	}

}
