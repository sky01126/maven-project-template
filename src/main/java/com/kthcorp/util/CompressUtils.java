/*
 * Copyright ⓒ [2017] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */
package com.kthcorp.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;

/**
 * CompressUtils.java
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @date 2017. 2. 4.
 * @see
 * @since 7.0
 */
public class CompressUtils {

	private static final int BUFFER = 2048;

	private CompressUtils() {
		// ignore.
	}

	public static final void createTarGZ(File source, File output) throws IOException {
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		GzipCompressorOutputStream gzos = null;
		TarArchiveOutputStream tos = null;
		try {
			fos = new FileOutputStream(output);
			bos = new BufferedOutputStream(fos);
			gzos = new GzipCompressorOutputStream(bos);
			tos = new TarArchiveOutputStream(gzos);
			tos.setLongFileMode(TarArchiveOutputStream.LONGFILE_POSIX);

			addFileToTarGz(tos, "", source);
		} finally {
			org.apache.commons.compress.utils.IOUtils.closeQuietly(tos);
			org.apache.commons.compress.utils.IOUtils.closeQuietly(gzos);
			org.apache.commons.compress.utils.IOUtils.closeQuietly(bos);
			org.apache.commons.compress.utils.IOUtils.closeQuietly(fos);
		}
	}

	/**
	 * @param tos
	 * @param source
	 * @param string
	 * @throws IOException
	 */
	private static void addFileToTarGz(TarArchiveOutputStream tos, String dir, File file) throws IOException {
		String entryName = dir + file.getName();
		TarArchiveEntry tarEntry = new TarArchiveEntry(file, entryName);
		tos.putArchiveEntry(tarEntry);

		if (file.isFile()) {
			org.apache.commons.compress.utils.IOUtils.copy(new FileInputStream(file), tos);
			tos.closeArchiveEntry();
		} else {
			tos.closeArchiveEntry();
			File[] children = file.listFiles();
			if (children != null) {
				for (File child : children) {
					addFileToTarGz(tos, entryName + "/", FileUtils.getFile(child.getAbsolutePath()));
				}
			}
		}
	}

}
