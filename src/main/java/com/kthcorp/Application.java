/*
 * Copyright ⓒ [2017] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */
package com.kthcorp;

import java.io.IOException;

import com.kthcorp.service.ProjectService;

public class Application {

	public static void main(String[] args) throws IOException {
		// BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		// String basePath = getBasePath(reader);
		// String groupId = getGroupId(reader);
		// String artifactId = getArtifactId(reader);
		// String projectName = getProjectName(reader, artifactId);
		// String description = getDescription(reader, artifactId);

		String path = "/Volumes/Development";
		String groupId = "com.kthcorp";
		String artifactId = "gradle-project";
		String name = "KTH Corp Project";
		String description = "KTH Corp Project";

		System.out.println("------------------------------------------------------");
		System.out.println("| GROUP_ID          : " + groupId);
		System.out.println("| ARTIFACT_ID       : " + artifactId);
		System.out.println("| PROJECT_NAME      : " + name);
		System.out.println("| DESCRIPTION       : " + description);
		System.out.println("------------------------------------------------------");

		ProjectService service = new ProjectService(path, groupId, artifactId, name, description);
		service.createBase();
		service.createSource();
		service.compress();
	}

	// /**
	// * @param reader
	// * @return Base Path
	// * @throws IOException
	// */
	// private static String getBasePath(BufferedReader reader) throws IOException {
	// System.out.print("Maven 생성 경로를 입력하세요 (ex. ./)> ");
	// String basePath = reader.readLine();
	// if (basePath == null || "".equals(basePath)) {
	// throw new IllegalStateException("Maven 생성 경로를 입력해주세요.");
	// }
	// return basePath;
	// }
	//
	// /**
	// * @param reader
	// * @return Group ID
	// * @throws IOException
	// */
	// private static String getGroupId(BufferedReader reader) throws IOException {
	// System.out.print("Maven Group ID를 입력하세요 (ex. com.kthcorp)> ");
	// String groupId = reader.readLine();
	// if (groupId == null || "".equals(groupId)) {
	// throw new IllegalStateException("Maven Group ID을 입력해주세요.");
	// }
	// if (groupId.split("[.]").length < 2) {
	// throw new IllegalStateException("Maven Group ID는 Java 패키지를 잘못 입력했습니다.(ex. com.kthcorp)");
	// }
	//
	// return groupId;
	// }
	//
	// /**
	// * @param reader
	// * @return Artifact ID
	// * @throws IOException
	// */
	// private static String getArtifactId(BufferedReader reader) throws IOException {
	// System.out.print("Maven Artifact ID를 입력하세요 (ex. test-project)> ");
	// String artifactId = reader.readLine();
	// if (artifactId == null || "".equals(artifactId)) {
	// throw new IllegalStateException("Maven Artifact ID을 입력해주세요.");
	// }
	// return artifactId;
	// }
	//
	// /**
	// * @param reader
	// * @param artifactId
	// * @return Project Name
	// * @throws IOException
	// */
	// private static String getProjectName(BufferedReader reader, String artifactId) throws IOException {
	// System.out.print("Maven Project Name을 입력하세요 (ex. test-project)> ");
	// String name = reader.readLine();
	// if (name == null || "".equals(name)) {
	// name = artifactId;
	// }
	// return name;
	// }
	//
	// /**
	// * @param reader
	// * @param artifactId
	// * @return Description
	// * @throws IOException
	// */
	// private static String getDescription(BufferedReader reader, String artifactId) throws IOException {
	// System.out.print("Maven Description을 입력하세요 (ex. test-project)> ");
	// String description = reader.readLine();
	// if (description == null || "".equals(description)) {
	// description = artifactId;
	// }
	// return description;
	// }

}
