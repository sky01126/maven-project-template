/*
 * Copyright ⓒ [2016] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */
package {GROUP_ID}.web.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

/**
 * HTTP Request Utils
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @since 7.0
 */
public class RequestUtils {

	private RequestUtils() {
		// ignore.
	}

	/**
	 * 브라우저가 Internet Explorer 인지 체크한다.
	 *
	 * @param request HttpServletRequest
	 * @return true / false
	 */
	public static final boolean isIE(final HttpServletRequest request) {
		String userAgent = request.getHeader("User-Agent");
		return isIE(userAgent);
	}

	/**
	 * 브라우저가 Internet Explorer 인지 체크한다.
	 *
	 * @param userAgent HTTP User-Agent
	 * @return true / false
	 */
	public static final boolean isIE(final String userAgent) {
		if (StringUtils.isNotEmpty(userAgent) && StringUtils.containsIgnoreCase(userAgent, "MSIE")) {
			return true;
		}
		return false;
	}

}
