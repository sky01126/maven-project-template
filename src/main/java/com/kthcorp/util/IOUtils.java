/*
 * Copyright ⓒ [2017] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */
package com.kthcorp.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

/**
 * IOUtils.java
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @see
 * @since 7.0
 */
public class IOUtils {

	public static final void closeQuietly(final Closeable c) {
		if (c != null) {
			try {
				c.close();
			} catch (final IOException ignored) {
				// ignore.
			}
		}
	}

	public static final String toString(InputStream in) throws IOException {
		StringBuffer sb = new StringBuffer();
		byte[] b = new byte[4096];
		for (int n; (n = in.read(b)) != -1;) {
			sb.append(new String(b, 0, n));
		}
		return sb.toString();
	}

}
