/*
 * Copyright ⓒ [2017] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */
package com.kthcorp.conf;

/**
 * Version.java
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @see
 * @since 7.0
 */
public class Version {

	public static final String PROJECT = "0.0.1-SNAPSHOT";

	/* Dependency POM */
	public static final String SPRING_BOOT = "1.4.4.RELEASE";

	public static final String JAVA = "1.7";

	public static final String CGLIB = "3.2.4";

	public static final String COMMONS_LANG3 = "3.5";

	public static final String COMMONS_NET = "3.5";

	public static final String EHCACHE_SPRING = "1.2.0";

	public static final String GUAVA = "20.0";

	public static final String HIKARI_CP = "2.6.0";

	public static final String JAKARTA_REGEXP = "1.4";

	public static final String JODA_TIME = "2.9.7";

	public static final String LOG4JDBC_LOG4J2_JDBC4 = "1.16";

	public static final String TOMCAT_EMBED = "7.0.73";

	public static final String MYBATIS = "3.4.2";

	public static final String MYBATIS_SPRING = "1.3.1";

	public static final String SQLJDBC4 = "4.0";

	public static final String MYSQL = "6.0.5";

	public static final String POSTGRESQL = "9.4.1212";

	public static final String LOMBOK = "1.16.12";

	/* Parent POM */
}
